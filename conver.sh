#!/bin/bash
FILE=$1

CMD="gs -sDEVICE=bbox -dBATCH -dNOPAUSE -c save pop -f "

VAL=`$CMD$FILE 2>&1 |grep -v HiRes| grep BoundingBox | awk '{print $2}'`

if [ "$VAL" == "-1" ]
    then CROP="-36 -16 2 -58"
    else CROP="-16 -16 -52 -58" 
fi

echo "CROP == $CROP"

CROPCMD="pdfcrop --margins '$CROP' --clip $FILE Cropped-$FILE"
echo $CROPCMD|sh
